let canvas = document.querySelector('#canvas1')
canvas.width = window.innerWidth
canvas.height = window.innerHeight
const c = canvas.getContext('2d')
let mouse = {
  x: undefined,
  y: undefined
}
const maxRadius = 40
let colorArray = [
  '#313A4B',
  '#F2E4C9',
  '#F2836B',
  '#8C0303',
  '#590202'
]
let circleArray = []

init()
animate()
console.log(canvas)

// track mouse position
window.addEventListener('mousemove', function (event) {
  mouse.x = event.x
  mouse.y = event.y
  console.log(mouse)
})

// resize canvas when window is resized
window.addEventListener('resize', function () {
  canvas.width = window.innerWidth
  canvas.height = window.innerHeight
  init()
})

function init () {
  circleArray = []
  for (let i = 0; i < 800; i++) {
    let radius = Math.random() * 4 + 1
    let x = Math.random() * (window.innerWidth - radius * 2) + radius
    let y = Math.random() * (window.innerHeight - radius * 2) + radius
    let dx = (Math.random() - 0.5) * 3
    let dy = (Math.random() - 0.5) * 3
    let color = 'rgb(' + Math.random() * 255 + ', ' + Math.random() * 255 + ', ' + Math.random() * 255 + ')'
    circleArray.push(new Circle(x, y, dx, dy, radius, color))
  }
}

function animate () {
  window.requestAnimationFrame(animate)
  c.clearRect(0, 0, window.innerWidth, window.innerHeight)
  for (let circle of circleArray) {
    circle.update()
  }
}

function Circle (x, y, dx, dy, radius) {
  this.x = x
  this.y = y
  this.dx = dx
  this.dy = dy
  this.radius = radius
  this.minRadius = radius
  this.color = colorArray[Math.floor(colorArray.length * Math.random())]

  this.draw = function () {
    c.beginPath()
    c.fillStyle = this.color
    c.strokeStyle = 'blue'
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
    c.fill()
    c.stroke()
  }

  this.update = function () {
    if (this.x + this.radius > window.innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx
    }
    if (this.y + this.radius > window.innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy
    }
    this.x += this.dx
    this.y += this.dy

    // interactivity
    if (Math.abs(mouse.x - this.x) < 50 &&
      Math.abs(mouse.y - this.y) < 50 &&
      this.radius < maxRadius) {
      this.radius += 1
    } else if (this.radius > this.minRadius) {
      this.radius -= 1
    }

    this.draw()
  }
}
